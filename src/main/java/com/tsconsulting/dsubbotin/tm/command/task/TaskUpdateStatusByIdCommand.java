package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskUpdateStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-status-by-id";
    }

    @Override
    public String description() {
        return "Update status task by id.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(id);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        Status status = Status.valueOf(statusValue);
        serviceLocator.getTaskService().updateStatusById(id, status);
        TerminalUtil.printMessage("[Updated task status]");
    }

}
