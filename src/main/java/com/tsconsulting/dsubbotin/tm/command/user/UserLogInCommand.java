package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class UserLogInCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-login";
    }

    @Override
    public String description() {
        return "User log in.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter login:");
        final String login = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter password:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        TerminalUtil.printMessage("Logged in.");
    }

}
