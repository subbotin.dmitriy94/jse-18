package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskUpdateStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-update-status-by-index";
    }

    @Override
    public String description() {
        return "Update status task by index.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter index:");
        /*try {*/
        final int index = TerminalUtil.nextNumber() - 1;
        serviceLocator.getTaskService().findByIndex(index);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        /*try {*/
        final Status status = Status.valueOf(statusValue);
        serviceLocator.getTaskService().updateStatusByIndex(index, status);
        TerminalUtil.printMessage("[Updated task status]");
        /*} catch (IllegalArgumentException e) {
            throw new UnknownStatusException();
        }*/
        /*} catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }*/
    }

}
