package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class UserIsAuthCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-auth";
    }

    @Override
    public String description() {
        return "Display authorized user.";
    }

    @Override
    public void execute() throws AbstractException {
        final User user = serviceLocator.getAuthService().getUser();
        TerminalUtil.printMessage(user.toString());
    }

}
