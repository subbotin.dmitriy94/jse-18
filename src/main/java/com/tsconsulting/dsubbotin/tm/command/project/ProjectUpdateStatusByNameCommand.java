package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownStatusException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectUpdateStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-update-status-by-name";
    }

    @Override
    public String description() {
        return "Update status project by name.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findByName(name);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        try {
            Status status = Status.valueOf(statusValue);
            serviceLocator.getProjectService().updateStatusByName(name, status);
            TerminalUtil.printMessage("[Updated project status]");
        } catch (IllegalArgumentException e) {
            throw new UnknownStatusException();
        }
    }

}
