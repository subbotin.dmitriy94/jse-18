package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.List;

public class UserListShowCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-list";
    }

    @Override
    public String description() {
        return "Display user list.";
    }

    @Override
    public void execute() throws AbstractException {
        final List<User> users = serviceLocator.getUserService().findAll();
        int index = 1;
        for (User user : users) TerminalUtil.printMessage(index++ + ". " + user);
    }

}
