package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class UserShowByLoginCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-show-by-login";
    }

    @Override
    public String description() {
        return "Display user by login.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter login:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findByLogin(login);
        showUser(user);
    }

}
