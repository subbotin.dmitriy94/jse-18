package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-unbind-from-project";
    }

    @Override
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(projectId);
        TerminalUtil.printMessage("Enter task id:");
        final String taskId = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(taskId);
        serviceLocator.getProjectTaskService().unbindTaskFromProject(projectId, taskId);
        TerminalUtil.printMessage("[Task untied to project]");
    }

}
