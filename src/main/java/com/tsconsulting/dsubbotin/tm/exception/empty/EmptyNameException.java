package com.tsconsulting.dsubbotin.tm.exception.empty;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Empty name entered.");
    }

}
