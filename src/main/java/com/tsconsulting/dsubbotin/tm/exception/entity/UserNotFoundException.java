package com.tsconsulting.dsubbotin.tm.exception.entity;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("User not found!");
    }

}
