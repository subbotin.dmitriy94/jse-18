package com.tsconsulting.dsubbotin.tm.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
