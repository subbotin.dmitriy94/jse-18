package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    void create(final String name) throws AbstractException;

    void create(final String name, final String description) throws AbstractException;

    void add(final Project project) throws AbstractException;

    void remove(final Project project) throws AbstractException;

    List<Project> findAll();

    List<Project> findAll(final Comparator<Project> comparator);

    void clear();

    Project findById(final String id) throws AbstractException;

    Project findByIndex(final int index) throws AbstractException;

    Project findByName(final String name) throws AbstractException;

    Project updateById(final String id, final String name, final String description) throws AbstractException;

    Project updateByIndex(final int index, final String name, final String description) throws AbstractException;

    Project startById(final String id) throws AbstractException;

    Project startByIndex(final int index) throws AbstractException;

    Project startByName(final String name) throws AbstractException;

    Project finishById(final String id) throws AbstractException;

    Project finishByIndex(final int index) throws AbstractException;

    Project finishByName(final String name) throws AbstractException;

    Project updateStatusById(final String id, final Status status) throws AbstractException;

    Project updateStatusByIndex(final int index, final Status status) throws AbstractException;

    Project updateStatusByName(final String name, final Status status) throws AbstractException;

}
