package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void create(final String name) throws AbstractException;

    void create(final String name, final String description) throws AbstractException;

    void add(final Task task) throws AbstractException;

    void remove(final Task task) throws AbstractException;

    List<Task> findAll();

    List<Task> findAll(final Comparator<Task> comparator);

    void clear();

    Task findById(final String id) throws AbstractException;

    Task findByIndex(final Integer index) throws AbstractException;

    Task findByName(final String name) throws AbstractException;

    Task removeById(final String id) throws AbstractException;

    Task removeByIndex(final int index) throws AbstractException;

    Task removeByName(final String name) throws AbstractException;

    Task updateById(final String id, final String name, final String description) throws AbstractException;

    Task updateByIndex(final int index, final String name, final String description) throws AbstractException;

    Task startById(final String id) throws AbstractException;

    Task startByIndex(final int index) throws AbstractException;

    Task startByName(final String name) throws AbstractException;

    Task finishById(final String id) throws AbstractException;

    Task finishByIndex(final int index) throws AbstractException;

    Task finishByName(final String name) throws AbstractException;

    Task updateStatusById(final String id, final Status status) throws AbstractException;

    Task updateStatusByIndex(final int index, final Status status) throws AbstractException;

    Task updateStatusByName(final String name, final Status status) throws AbstractException;

}
