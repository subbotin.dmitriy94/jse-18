package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskToProject(final String projectId, final String taskId) throws AbstractException;

    Task unbindTaskFromProject(final String projectId, final String taskId) throws AbstractException;

    List<Task> findAllTasksByProjectId(final String id) throws AbstractException;

    Project removeProjectById(final String id) throws AbstractException;

    Project removeProjectByIndex(final int index) throws AbstractException;

    Project removeProjectByName(final String name) throws AbstractException;

}
