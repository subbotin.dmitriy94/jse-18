package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;

import java.util.List;

public interface IUserRepository {

    void add(final User user);

    void remove(final User user);

    List<User> findAll();

    User findById(final String id) throws AbstractException;

    User findByLogin(final String login) throws AbstractException;

    User removeById(final String id) throws AbstractException;

    User removeByLogin(final String login) throws AbstractException;

    boolean isLogin(final String login);

}
