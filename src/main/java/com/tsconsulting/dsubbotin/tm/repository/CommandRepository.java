package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractSystemCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return arguments.get(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractSystemCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandNames() {
        final List<String> commandNames = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (name != null && !name.isEmpty()) commandNames.add(name);
        }
        return commandNames;
    }

    @Override
    public Collection<String> getCommandArgs() {
        final List<String> commandArgs = new ArrayList<>();
        for (final AbstractSystemCommand command : arguments.values()) {
            final String arg = command.arg();
            if (arg != null && !arg.isEmpty()) commandArgs.add(arg);
        }
        return commandArgs;
    }

    @Override
    public void add(final AbstractCommand command) {
        final String name = command.name();
        if (name != null) commands.put(name, command);
        if (command instanceof AbstractSystemCommand) {
            final String arg = ((AbstractSystemCommand) command).arg();
            if (arg != null) arguments.put(arg, ((AbstractSystemCommand) command));
        }
    }

}
