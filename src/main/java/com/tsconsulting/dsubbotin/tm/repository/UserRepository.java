package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public void add(final User user) {
        users.add(user);
    }

    @Override
    public void remove(final User user) {
        users.remove(user);
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findById(final String id) throws AbstractException {
        for (User user : users) {
            if (id.equals(user.getId())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User findByLogin(final String login) throws AbstractException {
        for (User user : users) {
            if (login.toLowerCase().equals(user.getLogin().toLowerCase())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User removeById(final String id) throws AbstractException {
        final User user = findById(id);
        users.remove(user);
        return user;
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        final User user = findByLogin(login);
        users.remove(user);
        return user;
    }

    @Override
    public boolean isLogin(final String login) {
        for (User user : users) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

}
