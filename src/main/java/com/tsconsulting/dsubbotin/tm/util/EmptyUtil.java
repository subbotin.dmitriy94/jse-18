package com.tsconsulting.dsubbotin.tm.util;

public interface EmptyUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

}
